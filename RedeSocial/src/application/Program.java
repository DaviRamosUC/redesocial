package application;

import entities.Amigo;
import entities.Post;
import entities.Rede;
import static java.lang.System.*;

public class Program {

	public static void main(String[] args) {
		
		Amigo fulano = new Amigo("Fulano");
		Amigo ciclano = new Amigo("Ciclano");
		Amigo beltrano = new Amigo("Beltrano");
		
		
		Rede paraiso = new Rede();
		paraiso.adicionarAmigo(fulano);
		paraiso.adicionarAmigo(ciclano);
		paraiso.adicionarAmigo(beltrano);
		
		Post bomdia = new Post("Bom dia!");
		Post boatarde = new Post("Boa tarde!");
		Post boanoite = new Post("Boa noite!");
		
		fulano.postar(bomdia);
		fulano.postar(boatarde);
		ciclano.postar(boanoite);
		
		beltrano.curtir(bomdia);
		beltrano.curtir(boatarde);
		beltrano.curtir(boanoite);
		fulano.curtir(boanoite);
		
		out.println("Timeline: ");
		out.println(paraiso.timeline());
		out.println("Post mais curtido de um usu�rio: ");
		Post maiscurtido = ciclano.retornaPostMaisCurtido();
		out.println(maiscurtido);
		out.println();
		out.println("Quem curtiu: " + maiscurtido.retornaNomesQueCurtiram());
		

	}

}
