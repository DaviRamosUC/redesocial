package entities;

import java.util.ArrayList;
import java.util.List;

public class Post {

	private String mensagem;
	private int curtidas=0;
	List<String> nomes = new ArrayList<>();

	public Post() {
	}

	public Post(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public int getCurtidas() {
		return curtidas;
	}

	public void incrementarCurtidas(String nome) {
		curtidas++;
		nomes.add(nome);
	}

	@Override
	public String toString() {
		return mensagem + ": " + curtidas + " curtidas";
	}

	public String retornaNomesQueCurtiram() {
		String nomess="";
		for (String s : nomes) {
			nomess+=" " +s;
		}
		return nomess;
	}
	
	
	
}
