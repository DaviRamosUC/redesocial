package entities;

import java.util.ArrayList;
import java.util.List;

public class Amigo {
	
	private String nome;
	static List<Post> posts = new ArrayList<>();

	public Amigo() {

	}

	public Amigo(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void postar(Post post) {
		posts.add(post);
	}
	
	public void curtir(Post post) {
		post.incrementarCurtidas(getNome());
	}

	public Post retornaPostMaisCurtido() {
		int maisCurtidas=0;
		Post maisCurtido=null;
		for (Post post : posts) {
			if (maisCurtidas<post.getCurtidas()) {
				maisCurtido=post;
			}
		}
		return maisCurtido;
	}
	
	
	
}
